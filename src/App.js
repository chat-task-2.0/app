import React from 'react';
import './App.css';
import { 
	BrowserRouter as Router,
	Switch, 
	Route, 
  Link 
} from 'react-router-dom';

import Login from './components/containers/Login';
import ChatRoom from './components/containers/ChatRoom';


function App() {

  return (
  	<Router>
    	<div className="App">
        <header className="App-header">
          <Link className="App-link" to='/login'>Login</Link>
          <Link className="App-link" to='/chatroom'>ChatRoom</Link>
        </header>

      		<Switch>
  	    		<Route path="/login" component={ Login } />
  	    		<Route path="/chatroom" component={ ChatRoom } />
      		</Switch>
	    </div>
  	</Router>
  );

} 

export default App;
