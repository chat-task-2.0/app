import React from 'react';
import io from 'socket.io-client';
import '../style/ChatRoom.css';

import ChatBoard from '../forms/ChatBoard';
import ChatInput from '../forms/ChatInput';
import UserList from '../forms/UserList';


class ChatRoom extends React.Component {

  constructor(props) {
    super(props);
    this.handleSend = this.handleSend.bind(this);

    this.state = {
      messages: [],
      users: [],
      socket: io('http://localhost:3000')
    };

    this.state.socket.on('connect', () => {
      console.log(localStorage.getItem('username'))
      this.state.socket.emit('username', localStorage.getItem('username'));

      this.state.socket.on('message', data => {
        let msgText = data.user + ": " + data.message
        console.log(data.user)
        this.setState({messages: [...this.state.messages, msgText]})
      });


      this.state.socket.on('username', data => {
        console.log(data)
        this.setState({users: [...this.state.users, data]})
      });

      this.state.socket.on('addUser', data => {
        console.log(data)
        this.setState({users: [...this.state.users, data]})
      });

      this.state.socket.on('addUsers', data => {
        this.setState({users: data})
      })

      this.state.socket.on('removeUser', data => {
        this.setState({users: this.state.users.filter(user => user !== data)})
      });

    });

  }

  handleSend(message) {
    this.state.socket.emit('message', message)
  }

  render() {
  	return (
  		<div className="ChatRoom">
	  		<div className="AppTop">
          <h4>CHAT</h4>
  	  			<div className="ChatBoardDiv">
  	  				<ChatBoard messages={this.state.messages} className="ChatBoard"/>	
  	  			</div>
            <h4>Online Users</h4>
  	  			<div className="UserListDiv">
  	  				<UserList users={this.state.users} className="UserList"/> 				
  	  			</div>
  	  		</div>	
	  		<div className="AppBottom">
	  			<ChatInput className="ChatInput" onSend={this.handleSend}/>
	  		</div>
  		</div>
	);
  };

}





export default ChatRoom;