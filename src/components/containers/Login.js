import React from 'react';
import LoginForm from '../forms/LoginForm';

const Login = () => {

	const handleLoginClicked = (result) => {
		console.log('triggered from loginform', result);
	}

	return (
		<div>
			<h4>Login to the chat</h4>

			<LoginForm click={ handleLoginClicked } />
		</div>
	)
};

export default Login; 