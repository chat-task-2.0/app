import React from 'react';
import '../style/ChatInput.css';
import { InputGroup, FormControl, Button, Form } from 'react-bootstrap';

class ChatInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			messagetext: '',
			username: localStorage.getItem('username')
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSend = this.handleSend.bind(this);
	}

	handleChange(event) {
		this.setState({messagetext: event.target.value});
	}

	handleSend(event) {
		let message = {
			user: this.state.username, 
			message: this.state.messagetext
		}

		this.props.onSend(message);
		this.setState({messagetext: ""});

	}

	render() {
		return (
			<div className="ChatInput">
				<div className="inputTextDiv">
					<InputGroup className="mb-3">
						<FormControl className="inputField" value={this.state.messagetext} onChange={this.handleChange} placeholder="Type a message ..." />
						<Button id="sendButton" onClick={this.handleSend}>Send</Button>
					</InputGroup>				
				</div>
			</div>
		);
	};
}

export default ChatInput;