import React, { useState} from 'react';
import '../style/LoginForm.css';
import { FormControl, Form, Col, Button } from 'react-bootstrap';

class LoginForm extends React.Component {

constructor(props) {
	super(props)
	this.onLoginClicked = this.onLoginClicked.bind(this)
	this.onUsernameChanged = this.onUsernameChanged.bind(this)
	this.state = {
		username: ''
	}
}

onLoginClicked(event)  {
	console.log('EVENT: ', event.target);
	localStorage.setItem('username', this.state.username)
	window.location.href = '/chatroom';
}

onUsernameChanged(event) {
	this.setState({username: event.target.value})
}

	render() {
	return (
		<form>
			<Form.Row>
				<Col sm={4}>
				</Col>
				<Col className="userInput" sm={4}>
					<FormControl type="text" placeholder="Enter username" onChange={ this.onUsernameChanged } />
				</Col>
				<Col  sm={4}>
				</Col>
			</Form.Row>

			<Form.Row>
				<Col sm={4}>
				</Col>
				<Col className="loginButton" sm={4}>
					<Button onClick={ this.onLoginClicked }>Login</Button>
				</Col>
				<Col  sm={4}>
				</Col>
			</Form.Row>
		</form>
);
	}


};

export default LoginForm;