import React from 'react';

function ChatBoard({messages}) {

	const listMessages = messages.map((message, i) => 
		<p key={"message-"+i}> {message} </p>
		);
	
	return (
		<div className="ChatBoard">
			{ listMessages }			
		</div>
	);
}

export default ChatBoard;