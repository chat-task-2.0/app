import React from 'react';


function UserList({users}) {

	const listUsers = users.map((user, i) => 
		<p key={"user-"+i}> {user} </p>
		);

	return (
		<div className="UserList">
			{ listUsers }
		</div>
	);
}

export default UserList;